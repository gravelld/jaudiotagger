# Forking details for elsten software branch

### `pom.xml` changes
A number of changes are require to allow deployment to our repositories:

```
$ git diff pom.xml
diff --git a/pom.xml b/pom.xml
index f5155434..4f283b62 100644
--- a/pom.xml
+++ b/pom.xml
@@ -28,7 +28,7 @@
    <modelVersion>4.0.0</modelVersion>
    <groupId>net.jthink</groupId>
    <artifactId>jaudiotagger</artifactId>
-   <version>2.2.6-SNAPSHOT</version>
+   <version>2.2.6.20200224-SNAPSHOT</version>
    <packaging>jar</packaging>
    <name>jaudiotagger</name>
    <description>
@@ -127,6 +127,13 @@
             <version>4.0.0</version>
          </plugin>
       </plugins>
+    <extensions>
+      <extension>
+        <groupId>org.kuali.maven.wagons</groupId>
+        <artifactId>maven-s3-wagon</artifactId>
+        <version>1.2.1</version>
+      </extension>
+    </extensions>
       <sourceDirectory>src</sourceDirectory>
       <testSourceDirectory>srctest</testSourceDirectory>
    </build>
@@ -184,10 +191,21 @@
       <url>https://github.com/ijabz/jaudiotagger</url>
       <tag>jaudiotagger-2.2.6</tag>
    </scm>
-   <distributionManagement>
-      <repository>
-         <id>bintray</id>
-         <url>https://api.bintray.com/maven/ijabz/maven/jaudiotagger</url>
-      </repository>
-   </distributionManagement>
+ <distributionManagement>
+    <snapshotRepository>
+      <id>maven-s3-snapshot-repo</id>
+      <url>s3://repo.elstensoftware.com/maven/snapshots</url>
+    </snapshotRepository>
+    <repository>
+      <id>maven-s3-release-repo</id>
+      <url>s3://repo.elstensoftware.com/maven/releases</url>
+    </repository>
+  </distributionManagement>
+  <repositories>
+    <repository>
+      <id>maven-s3-release-repo</id>
+      <url>s3://repo.elstensoftware.com/maven/releases</url>
+    </repository>
+  </repositories>
+  
 </project>
```

`2.2.6.20200224-SNAPSHOT` is an example varsion - replace it as required.

### Deployment
(After the `pom.xml` is edited).

mvn deploy